var data = require('../../TestResources/DocprodData.js');
var Objects = require(__dirname + '/../../repository/DocprodPages.js');
//var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var ks = require('node-key-sender');
//var path = require('../../Downloads');
var fs = require('fs');
//var email = require(__dirname + '/../../node_modules/emailjs');
var browser;
var Documentproduction, Adhocpage,Administrationtab,CTASearch;

function initializePageObjects(browser, callback) {
    var DPPage = browser.page.DocprodPages();
    Documentproduction = DPPage.section.DocumentProductionPage;
    Adhocpage = DPPage.section.AdhocFieUploadPage;
    Administrationtab = DPPage.section.AdminstrationPage;
    CTASearch=DPPage.section.DocumentProductionPage;
    callback();
}


module.exports = function () {

    // Scenario1
     this.Given(/^User Navigate to DocProd Home Page$/, function () {
     var URL;
     browser = this;
     var execEnv = data["TestingEnvironment"];
     console.log('Test Environment: ' + execEnv);
     if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
     URL = data.DocProdURL;
     initializePageObjects(browser, function () {
     browser.maximizeWindow()
     .deleteCookies()
     .url(URL);
     browser.timeoutsImplicitWait(20000);
     });
     }
     });

     this.When(/^User navigates to Adhoc file upload Page$/, function () {
     var URL;
     browser = this;
     Documentproduction.waitForElementVisible('@Adhocfileuploadlink', data.longWait)
     .click('@Adhocfileuploadlink');
     browser.timeoutsImplicitWait(20000);

     });

     this.Then(/^Verify that a ppt cannot access Outgoing Print Files option$/, function () {
     var URL, res;
     browser = this;

     Adhocpage.waitForElementVisible('@OutgoingPrintFiles', data.longWait)
         browser.timeoutsImplicitWait(20000);
     Adhocpage.getText('@OutgoingPrintFiles', function (res) {
     console.log(res.value);

     if(res.value=="You do not have permissions")
     {
     console.log("passed");
     }
     else{
     console.log("failed");
     }

     });
     });


    //Scenario2

     this.Given(/^User navigate to DocProd Home Page$/, function () {
     var URL;
     browser = this;
     var execEnv = data["TestingEnvironment"];
     console.log('Test Environment: ' + execEnv);
     if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
     URL = data.DocProdURL;
     initializePageObjects(browser, function () {
     browser.maximizeWindow()
     .deleteCookies()
     .url(URL);
     browser.timeoutsImplicitWait(20000);
     });
     }
     });

     this.When(/^User navigates to Administration tab$/, function () {

     browser = this;
     Administrationtab.waitForElementVisible('@AdministrationTab', data.longWait)
     .click('@AdministrationTab');
     browser.timeoutsImplicitWait(20000);

     });

     this.Then(/^Verify that a ppt cannot access edit tab under Manage administrator$/, function () {

     var searchcontent, res1;
     browser = this;

     Administrationtab.waitForElementVisible('@ManageAdministrators', data.longWait)
     .click('@ManageAdministrators');
     browser.timeoutsImplicitWait(20000);

     Administrationtab.waitForElementVisible('@SearchBox', data.longWait)
     .setValue('@SearchBox', data.Searchboxdata)


     Administrationtab.waitForElementVisible('@FindNow', data.longWait)
     .click('@FindNow');
     browser.timeoutsImplicitWait(20000);


     Administrationtab.waitForElementVisible('@Elementselection', data.longWait)
     .click('@Elementselection');
     browser.timeoutsImplicitWait(20000);


     // if (browser.useXpath().waitForElementVisible("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]",data.longWait))
     //
     // {
     //     console.log("User is having SuperUser access");
     //    //browser.useXpath().waitForElementVisible("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]",data.longWait)
     //     browser.useXpath().click("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]");
     //     browser.pause(10000);
     // }
     // else
     // {
     //     console.log("User doesn't have SuperUser access");
     //     browser.pause(10000);
     // }

     //browser.useXpath().expect.element("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]").to.be.present()
     //var bool= browser.useXpath().waitForElementVisible("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]", 5000, true);
     // verify.visible("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]");

     //console.log(bool.value);
     //browser.element('xpath', "//h3[contains(.,'" + data.Searchboxdata + "')]/../a[contains(text(),'Edit')]".unavailable, function (result) {
     //browser.pause(5000);

     //if (result.value.ELEMENT || (browser.useXpath().waitForElementVisible("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]",2000)))
       if("//h3[contains(.,'" + data.Searchboxdata + "')]/../a[contains(text(),'Edit')]".unavailable)
     {

     console.log("User doesn't have SuperUser access");
     browser.pause(10000);
     }
     else
     {
     console.log("User is having SuperUser access");
     //    //browser.useXpath().waitForElementVisible("//h3[contains(.,'"+data.Searchboxdata+"')]/../a[contains(text(),'Edit')]",data.longWait)
     browser.useXpath().click("//h3[contains(.,'" + data.Searchboxdata + "')]/../a[contains(text(),'Edit')]");
     browser.pause(10000)
     }
     });


//scenario3

   this.Given(/^User Navigate to DocProd Home Page using iframe URL$/, function () {
        var URL;
        URL=data["iframeURL"];
        browser = this;
       // Documentproduction.waitForElementVisible('@Adhocfileuploadlink', data.longWait)

        browser.maximizeWindow()
            .deleteCookies()
            .url(URL)
       .timeoutsImplicitWait(20000);
      // Documentproduction.waitForElementVisible('@Adhocfileuploadlink', data.longWait)
   });


    this.Then(/^Verify Administration Foundation title is not in home page$/, function () {
        var URL;
        browser = this;

       // this.waitForElementVisible('body', 5000)
           // this.frame(0)
            this.pause(3000)

        .frame("my-frame", function() {
            var natija;
            this.waitForElementPresent('span#WebPartTitle_twp1255338445', 2000,function (natija) {
                //console.log(natija.value);
                if (natija.value) {


                    console.log("Able to access app from iframe - Scenario failed");
                }

                else {

                    console.log("Not able to access app from iframe");
                }
            });

        });
        //browser.timeoutsImplicitWait(20000);
     // browser.waitForElementPresent('span#WebPartTitle_twp1255338445',2000);

      //elementPresent('span#WebPartTitle_twp1255338445', data.longWait);

     // waitForElementVisible('span#WebPartTitle_twp1255338445', data.longWait);
      // browser.useXpath().waitForElementPresent("//a[contains(text(),'Click here to go to AdHoc File upload page')]",10000);

      // browser.useXpath().expect.element("//a[contains(text(),'Click here to go to AdHoc File upload page").to.be.visible;
        //
      //  Documentproduction.getText('@Adhocfileuploadlink', function (res1) {
      //      console.log(res1.value);
      //  })


       // Adhocpage.getText('@OutgoingPrintFiles', function (res) {
        //    console.log(res.value);

          //  if(res.value=="You do not have permissions1.")
          //  {
           //     console.log("passed");
           // }
           // else{
           //     console.log("failed");
           // }

        });

//scenario4

    this.Given(/^User Navigate to DocProd url$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            URL = data.DocProdURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(20000);
            });
            browser.assert.title('CallToAction')
                .pause(2000);

        }
    });



    this.When(/^Change https to http and try opening the URL$/, function () {
        var URL;
        browser = this
        .pause(2000);
        browser.resizeWindow(500,500)
            .deleteCookies()
        .pause(2000);
        console.log('Changed URL : ' + data.httpURL );
        URL=data.httpURL;
        console.log('Changed URL from https to http');
        browser.maximizeWindow()
            .deleteCookies()
            .url(URL);
        browser.timeoutsImplicitWait(20000);
    });


    //scenario5

    this.Given(/^User Navigate to DocProd url when servers are down$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            URL = data.DocProdURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(20000);
            });
        }
    });

    this.Then(/^User should not see any code being displayed$/, function () {
        var URL, bool1;
        var serdowntxt;
        browser = this;
       // Documentproduction.waitForElementVisible('@CTAServerdown', data.longWait)
      // bool1=  browser.assert.containsText('span#WebPartTitle_twp1255338445',"Call To Action");
       //browser.pause(2000)
       // console.log(bool1.value);

        browser.element('xpath', '//div[@id="main-message"]/h1', function(result4){
            if(result4.status != -1){
                //Element exists, do something
                console.log("Servers are down");
            browser.getText('#main-message > h1',function(serdowntxt){
                console.log(serdowntxt.value + "  Thus the code is not displayed");
                });
               // console.log(result4.value);
            } else{
                //Element does not exist, do something else
                console.log("DocProd servers are up");
            }
        });


            // Documentproduction.getText('@CTAServerdown', function (result3) {
            //     console.log(result3.value);
            //     if (result3.value == "Error") {
            //         console.log("Servers are down");
            //     }
            //     //  else

                //{console.log();




    });


        //Scenario6

         this.Given(/^User Navigate to DocProd URL$/, function () {
         var URL;
         browser = this;
         var execEnv = data["TestingEnvironment"];
         console.log('Test Environment: ' + execEnv);
         if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
         URL = data.DocProdURL;
         initializePageObjects(browser, function () {
         browser.maximizeWindow()
         .deleteCookies()
         .url(URL);
         browser.timeoutsImplicitWait(20000);
         });
         }
         });
         this.When(/^User changes the Client name in the URL to any other random name$/, function () {

         var URL1;
         browser = this;
         var execEnv = data["TestingEnvironment"];
         console.log('Test Environment: ' + execEnv);
         if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
         URL1 = data.IncorrectURL;
         initializePageObjects(browser, function () {
         browser.maximizeWindow()
         .deleteCookies()
         .url(URL1);
         browser.timeoutsImplicitWait(20000);

         });
         }

         });

         this.Then(/^Verify that user is not be able to access the profile summary of this client$/, function () {
         var result;
         browser = this;

         Documentproduction.waitForElementVisible('@Errorpage', data.longWait)
         browser.timeoutsImplicitWait(20000);
         Documentproduction.getText('@Errorpage', function (result) {
         console.log(result.value);

         if(result.value=="Unexpected Error")
         {
         console.log("Sceanrio passed. URL is expected to fail");
         }
         else{
         console.log("Scenario failed. Incorrect URL should not have loaded");
         }
         browser.pause(5000);
         });
         });



    //Scenario7
    this.Given(/^Open Docprod CTA screen in AF$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            URL = data.DocProdURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(20000);
            });
        }
    });

    this.When(/^Enter ssn within CTA searchbox and Click on Search button$/, function () {

        browser = this;
        CTASearch.waitForElementVisible('@CTASearchbox', 1000)
            .setValue('@CTASearchbox',data.CTASearchboxdata);

        browser.timeoutsImplicitWait(2000);

        CTASearch.waitForElementVisible('@CTASearchbtn', 2000)

            .click('@CTASearchbtn');
        browser.timeoutsImplicitWait(2000);


    });

    this.Then(/^The search field must not disclose to the user the recently searched participant SSNs$/, function () {
        var URL, res;
        browser = this;

        CTASearch.waitForElementVisible('@CTASearchbox', 1000)
            .clearValue('@CTASearchbox');
        browser.pause(2000);
       CTASearch.waitForElementVisible('@CTASearchbox', 1000)
        .click('@CTASearchbox');
        browser.pause(2000)

      .keys('9')
        .keys('0')

     .keys('1');
        browser.pause(9000);


        CTASearch.getText('@CTASearchbox', function (result7) {
            console.log(result7.value);

            if(result7.value=="901000085")
            {
                console.log("Scenario failed. ");
            }
            else{
                console.log("Scenario passed. ");
            }


    });
    });


    //Scenario8

    this.Given(/^User Navigate to DocProd URL$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QAI" || execEnv.toUpperCase() == "CITEST" || execEnv.toUpperCase() == "PROD" || execEnv.toUpperCase() == "CSO") {
            URL = data.DocProdURL;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(20000);
            });
        }
    });
    this.When(/^User navigates to Adhoc Batch file load page$/, function () {

        browser = this;
        Documentproduction.waitForElementVisible('@Adhocfileuploadlink', data.longWait)
            .click('@Adhocfileuploadlink');
        browser.timeoutsImplicitWait(20000);

        Adhocpage.waitForElementVisible('@Choosefile', data.longWait)
            .click('@Choosefile');
        browser.timeoutsImplicitWait(20000);

        browser.setValue('input[type="file"]',require('path').resolve('C://test.exe'));
        browser.pause(2000);

        Adhocpage.waitForElementVisible('@RequestBatchOption', data.longWait)
            .click('@RequestBatchOption');

        Adhocpage.waitForElementVisible('@UploadFileButton', data.longWait)
            .click('@UploadFileButton');
        browser.timeoutsImplicitWait(20000);


    });



    this.Then(/^Verify that user is not be able to upload file with .exe format$/, function () {
        var result2;
        browser = this;

        Adhocpage.waitForElementVisible('@FileNamecell', data.longWait)
        browser.timeoutsImplicitWait(20000);
        Adhocpage.getText('@FileNamecell', function (result2) {
            console.log(result2.value);

            if(result2.value=="Zoom_launcher.exe")
            {
                console.log("Sceanrio failed. File got uploaded");
            }
            else{
                console.log("Scenario passsed. File did not upload");
            }
            browser.pause(5000);
        });
    });




}

