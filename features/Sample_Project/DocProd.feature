Feature: Security Scenarios for DocProd
  @Scenario1
  Scenario: Verify that a ppt without admin/proxy group, cannot access administration page
    Given User Navigate to DocProd Home Page
    When  User navigates to Adhoc file upload Page
    Then Verify that a ppt cannot access Outgoing Print Files option


  @Scenario2
  Scenario: Verify that a ppt without Superuser status, cannot edit his roles and privileges
    Given User navigate to DocProd Home Page
    When  User navigates to Administration tab
    Then Verify that a ppt cannot access edit tab under Manage administrator

  @Scenario3
  Scenario: To verify whether AF core opens in an iframe
    Given User Navigate to DocProd Home Page using iframe URL
    Then  Verify Administration Foundation title is not in home page

  @Scenario4
  Scenario: To verify security of DocProd application
    Given User Navigate to DocProd url
    Then  Change https to http and try opening the URL


  @Scenario5
  Scenario:  Error handling-Testing for Stack Traces
    Given User Navigate to DocProd url when servers are down
    Then  User should not see any code being displayed

  @Scenario6
  Scenario: Verify that a ppt is able to modify the parameters in the URL to access another user's information
    Given User Navigate to DocProd URL
    When User changes the Client name in the URL to any other random name
    Then Verify that user is not be able to access the profile summary of this client

  @Scenario7
  Scenario:To verify that the application does not remember sensitive data
    Given Open Docprod CTA screen in AF
    When Enter ssn within CTA searchbox and Click on Search button
    Then The search field must not disclose to the user the recently searched participant SSNs

  @Scenario8
  Scenario: Verify that application does not allow the upload of unexpected file types
    Given User Navigate to DocProd URL
    When User navigates to Adhoc Batch file load page
    Then Verify that user is not be able to upload file with .exe format









