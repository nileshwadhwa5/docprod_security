/**
 * Created by nilesh-wadhwa on 12/12/2017.
 */

module.exports = {
    sections: {

        DocumentProductionPage: {
            selector: 'body',
            elements: {
                //QAI
                Adhocfileuploadlink: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Click here to go to AdHoc File upload page')]"},

                CTA: {locateStrategy: 'css selector',selector: "span#WebPartTitle_twp1255338445" },
                Errorpage: {locateStrategy: 'xpath',selector: "//span[@title='Unexpected Error']"},
                CTASearchbox : {locateStrategy: 'xpath', selector: "//input[@name=\"ctl00$TopazWebPartManager1$twp1255338445$ctl08\"]"},
                CTASearchbtn: {locateStrategy: 'xpath', selector:"//input[@name=\"ctl00$TopazWebPartManager1$twp1255338445$ctl15\"]"},
                //CTAServerdown: {locateStrategy: 'xpath',selector:"//div[@id="main-message"]/h1"},
            }
        },

        AdhocFieUploadPage:{
            selector: 'body',
            elements: {
                //QAI
                //OutgoingPrintFiles: {locateStrategy: 'css selector'
                // ,selector: "css=span#ctl00_TopazWebPartManager1_twp1922030610_lblAutomateResubmission"},
                //OutgoingPrintFiles: {selector: "#ctl00_TopazWebPartManager1_twp1922030610_lblAutomateResubmission"},
                OutgoingPrintFiles: {selector: "#ctl00_TopazWebPartManager1_twp1922030610"},
                Choosefile: {locateStrategy: 'xpath' ,selector: "//input[@name='ctl00$TopazWebPartManager1$twp668804750$flUpdAdhoc']"},
                UploadFileButton: {locateStrategy: 'xpath' ,selector: "//input[@name='ctl00$TopazWebPartManager1$twp668804750$btnUploadFile']"},
                RequestBatchOption: {locateStrategy: 'xpath' ,selector: "//input[@name='ctl00$TopazWebPartManager1$twp668804750$ctl03']"},
                FileNamecell: {locateStrategy: 'xpath' ,selector: "//table[@id='ctl00_TopazWebPartManager1_twp2071544545_dataGrid']/tbody/tr[2]/td[4]"},


            }



        },
        AdminstrationPage:{
            selector: 'body',
            elements: {

                AdministrationTab: {locateStrategy: 'xpath',selector: "//a[@href='https://afcore-qai.mercer.com/ApplicationsLanding.tpz']"},
                ManageAdministrators: {locateStrategy: 'xpath',selector: "//a[contains(text(),'Manage Administrators')]"},
                SearchBox:{locateStrategy: 'xpath',selector: "//*[@id='ctl00_TopazWebPartManager1_twp448343914_displayGrid_ctl54_searchValue']"},
                FindNow: {locateStrategy: 'xpath',selector: "//*[@id='ctl00_TopazWebPartManager1_twp448343914_displayGrid_ctl54_findButton']"},
                Elementselection: {locateStrategy: 'xpath',selector: "//table[@class='geostandardgridview']/tbody/tr[2]/td/a"},
                // Editlink: {locateStrategy: 'xpath',selector: "//a[@href='https://afcore-qai.mercer.com/Security/ModifyAdministrator.tpz?ParticipantId=3215222']"},



            }
        },
        }

};

